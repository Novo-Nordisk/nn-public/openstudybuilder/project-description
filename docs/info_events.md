# Events

Overview

Date | Event | Status 
-- | -- | --
3-4 March 2025 | ACDM - Prague, Czech Republic | Scheduled
10 March 2025 | Community Meeting (virtual) | Scheduled
13 March 2025 | PHUSE SDE Frankfurt, Germany | Scheduled
24 March 2025 | OSB-Trail-SystemEngineers (APIs) | Scheduled
7 April 2025 | Community Meeting (virtual) | Scheduled
13 May 2025 | Workshop at CDISC EU Interchange - Geneva, Switzerland | Scheduled
14-15 May 2025 | CDISC EU Interchange - Geneva, Switzerland | Scheduled
23 October  2025 | PHUSE SDE Copenhagen, Denmark | Planned
16-19 November 2025 | PHUSE EU Connect - Hamburg, Germany | Planned

## ACDM - Prague

**Digitalizing the study setup process from Protocol to Data Collection Specifications using OpenStudyBuilder**

(3-4 March 2025 - Prague, Czech Republic)

by Camilla Kehler and Ana Calduch Arques, Novo Nordisk

Discover how we at Novo Nordisk digitalizing the study protocol using OpenStudyBuilder to streamlining the study specification process from Schedule of Activities (SoA) via the data collection specification to the study submission specification. Maintain the SoA as part of a structured study specification, catering for the distinct study purposes: from high level display in the protocol (Protocol SoA), through the semantic data observations level to be collected in the study (Detailed SoA), to the data specification level linking to SDTM as well as ADAM (Operational SoA). The different levels of the SoA plays a key role when working with biomedical concepts and is integrated in the data model linked to the corresponding biomedical concept definitions in OpenStudyBuilder library module. This way and end-to-end connectivity is enabling the study setup automation.

## Community Meeting (virtual)

(10 March 2025, virtual event)

Join us at the OpenStudyBuilder community meeting! Don't miss out on the opportunity to ask any questions to us! We can demonstrate various functionality and provide insightful answers. We can discuss on a wide range of subjects.

You can register for the event [here](https://www.linkedin.com/events/openstudybuildercommunitymeetin7291012867393298434/comments/){target=_blank}.

## PHUSE SDE Frankfurt

(13 March 2025 - Frankfurt, Germany)

Katja will be available at the PHUSE Single Day Event - you can use the opportunities to talk about the OpenStudyBuilder.

## OSB-Trail-SystemEngineers (APIs)

(24 March 2025, 15:00-16:00 CET, virtual event)

Join the next session in our COSA Collaboration Team for OSB-Trail-SystemEngineers! This time we will focus on API utilization.

Meeting information are available [here](https://www.linkedin.com/events/osb-trail-systemengineers-focus7300543836559208448/comments/){target=_blank}.

## Community Meeting (virtual)

(7 April 2025, virtual event)

Join us at the OpenStudyBuilder community meeting! Don't miss out on the opportunity to ask any questions to us! We can demonstrate various functionality and provide insightful answers. We can discuss on a wide range of subjects.

You can register for the event [here](https://www.linkedin.com/events/openstudybuildercommunitymeetin7291013518252728320/comments/){target=_blank}.

## Workshop at CDISC EU Interchange 

(13 May 2025 - Geneva, Switzerland with Mikkel Traun, Nicolas de Saint-Jorre and Katja Glass)

In this interactive workshop, participants will gain hands-on experience in designing a clinical study using OpenStudyBuilder, with a focus on the schedule of activities. Explore how the CDISC 360i envisioned "Design" and "Build" processes are implemented within the tool. Learn to create visits, assign activities, and export your study to USDM. Experience the rendering into an M11 protocol. Additionally, select data specifications for chosen activities, then export and review the structure definition in define.xml format. This workshop offers a practical introduction to the steps and processes, providing a first impression of how this workflow can transform clinical study development.

You can register for the event [here](https://www.cdisc.org/events/interchange/2025-cdisc-tmf-europe-interchange){target=_blank}.


## CDISC EU Interchange 

(14-15 May 2025 - Geneva, Switzerland)

We will be at the CDISC EU Interchange conference in Geneva. We will do demonstrations at the COSA booth and maybe have a presentation or poster about the OpenStudyBuilder.

## PHUSE SDE Copenhagen

(23 October  2025 - Copenhagen, Denmark)

We will very likely be available at the PHUSE Single Day Event.

## PHUSE EU Connect

(16-19 November 2025 - Hamburg, Germany)

We will very likely be available at the PHUSE EU Connect conference in Hamburg.