# Newsletters

The OpenStudyBuilder Newsletter is published monthly on [LinkedIn](https://www.linkedin.com/newsletters/openstudybuilder-6990328054849916928/){target=_blank}. 

### Resum&eacute; 2024

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7287831972062609408" height="910" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Community

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7275809283101888512" height="990" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Release 0.12 & Events Recap

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7269729982212411393" height="995" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Release 0.11.2, OSB Trail Controlled Terminology & Events

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7252706738343219202" height="950" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Release 0.10

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7239986316774420480" height="863" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Roadmap

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7224003065664712706" height="760" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Release of OpenStudyBuilder 0.9 & The OpenStudyBuilder Hub

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7213446330210267136" height="780" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Recap of the CDISC Interchange: EDC Workshop, MDR Meetup, and Conference Summary

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7196163588414144513" height="910" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Events & Collaboration, Shaping the Future, Release 0.8.1

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7183023415950200832" height="685" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Status, MDR collaboration, Upcoming Events

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7169238927038365696" height="775" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>

### Ecosystem, Dashboard, EDC Integration & Workshop, Release 0.7.3

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:ugcPost:7158353239233507329" height="940" width="100%" frameborder="0" allowfullscreen="" title="Embedded post"></iframe>
