# Status

The OpenStudyBuilder is an open source project which was released in October 2022 for the public. The agile development contineusly releases new version in GitLab. Since October 2022 Novo Nordisk performed two pilot studies to enhance the solution further. On October 31, 2023, Novo Nordisk launched of the first business release of the internal OpenStudyBuilder. From now on all phase 2 and 3 studies in Novo Nordisk use the OpenStudyBuilder to generate study protocol parts.

## Communication

We use the following communication types:

- Workshops (please see [events](./info_events.md) for upcoming ones)
- Presentations (please see [events](./info_events.md) for upcoming ones)
- Virtual connects (OpenStudyBuilder Q&A) 
- Direct meetings with companies to show the tool and discuss collaborations
- Newsletter (subscribe on LinkedIn [here](https://www.linkedin.com/newsletters/openstudybuilder-6990328054849916928/){target=_blank})
- Slack (join [here](https://join.slack.com/t/openstudybuilder/shared_invite/zt-19mtauzic-Jvrhtmy7hGstgyiIvB1Wsw){target=_blank})
- Community mail (<a href="mailto:OpenStudyBuilder@gmail.com">OpenStudyBuilder@gmail.com</a>)

